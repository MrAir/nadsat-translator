package com.example.aired.nadsattranslator;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class TranslatorActivitySpanish extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    EditText et1;
    TextView tx1, output;
    Button bt2, translate;
    ImageButton exit, stay, info;

    FirebaseDatabase database;
    DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translator_spanish);
        tx1 = findViewById(R.id.tituloapp);
        output = findViewById(R.id.salida);
        et1 = findViewById(R.id.introducir);
        bt2 = findViewById(R.id.english);
        translate = findViewById(R.id.translatebutton);

        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetter(source.charAt(i))&&!Character.isSpaceChar(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };
        et1.setFilters(new InputFilter[]{filter});

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();

        translate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = et1.getText().toString();
                input = input.toLowerCase();
                input = input.trim();
                firebaseTranslateSearch(input);
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), TranslatorActivity.class);
                startActivity(myIntent);
            }
        });

        infoButton();

    }

    private void firebaseTranslateSearch(String input) {
        if (input.equals("")) {
            Toast.makeText(TranslatorActivitySpanish.this, "Escribe algo", Toast.LENGTH_SHORT);
        } else {
            String firstletter = String.valueOf(input.charAt(0));
            myRef.child("nadsatespanol").child(firstletter).child(input).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {

                    if (snapshot.exists()) {

                        String value = snapshot.getValue(String.class);
                        output.setText(value);

                    } else {
                        Toast.makeText(TranslatorActivitySpanish.this, "Palabra no encontrada", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    public void onBackPressed() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_exit);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.fui_transparent)));

        stay = dialog.findViewById(R.id.ib1);
        exit = dialog.findViewById(R.id.ib2);

        stay.setEnabled(true);
        exit.setEnabled(true);


        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void infoButton(){
        info = findViewById(R.id.infobutton);
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_info_spanish);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.fui_transparent)));
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }
}
